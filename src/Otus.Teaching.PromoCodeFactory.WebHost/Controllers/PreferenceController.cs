﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IEfRepository<Preference> _preferenceBaseRepository;

        public PreferenceController(IEfRepository<Preference> preferenceBaseRepository)
        {
            _preferenceBaseRepository = preferenceBaseRepository;
        }
        
        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Preference>> GetPreferenceAsync()
        {
            var preferences = await _preferenceBaseRepository.GetAllAsync();
            var result = preferences.ToList();

            return result;
        }
        
        /// <summary>
        /// Получить предпочтение по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Preference>> GetPreferenceByIdAsync(Guid id)
        {
            var preference = await _preferenceBaseRepository.GetByIdAsync(id);
            if (preference == null) return NotFound();
            
            return preference;
        }
    }
}
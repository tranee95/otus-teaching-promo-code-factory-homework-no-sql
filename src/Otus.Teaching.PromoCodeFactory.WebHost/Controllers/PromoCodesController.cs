﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class PromoCodesController : ControllerBase
    {
        private readonly IEfRepository<PromoCode> _promoCodeBaseRepository;
        private readonly IEfRepository<Customer> _customerBaseRepository;
        private readonly IEfRepository<Preference> _preferenceBaseRepository;

        public PromoCodesController(
            IEfRepository<PromoCode> promoCodeBaseRepository,
            IEfRepository<Customer> customerBaseRepository,
            IEfRepository<Preference> preferenceBaseRepository)
        {
            _promoCodeBaseRepository = promoCodeBaseRepository;
            _customerBaseRepository = customerBaseRepository;
            _preferenceBaseRepository = preferenceBaseRepository;
        }

        [HttpGet]
        public async Task<List<PromoCode>> GetPromoCodesAsync()
        {
            var result = await _promoCodeBaseRepository.GetAllAsync();
            return result.ToList();
        }

        [HttpGet]
        public async Task<PromoCode> GetPromoCodeAsync(Guid id)
        {
            return await _promoCodeBaseRepository.GetByIdAsync(id);
        }

        [HttpPost]
        public async Task<PromoCode> AddPromoCodeAsync(PromoCode promoCode)
        {
            promoCode.GeneratedId();
            await _promoCodeBaseRepository.AddAsync(promoCode);

            return promoCode;
        }

        [HttpPost]
        public async Task<IActionResult> GivePromocodesToCustomersWithPreferenceAsync(
            string preferensName,
            PromoCode promoCode)
        {
            promoCode.GeneratedId();
            
            var preference = _preferenceBaseRepository.Find(s => s.Name == preferensName);
            var customers = _customerBaseRepository.Where(s =>
                s.CustomerPreferences.Any(x => x.Preference.Id == preference.Id));

            foreach (var customer in customers)
            {
                customer.PromoCodes.Add(promoCode);
                await _customerBaseRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}
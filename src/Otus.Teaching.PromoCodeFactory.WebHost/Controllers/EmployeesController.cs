﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEfRepository<Employee> _employeeBaseRepository;

        public EmployeesController(IEfRepository<Employee> employeeBaseRepository)
        {
            _employeeBaseRepository = employeeBaseRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeBaseRepository.GetAllAsync();
            var result = 
                employees
                    .Select(x => new EmployeeShortResponse(x.Id, x.FullName, x.Email))
                    .ToList();

            return result;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeBaseRepository.GetByIdAsync(id);
            if (employee == null) return NotFound();
            
            return new EmployeeResponse(employee);
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="employeeItem"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> Create([FromBody]EmployeeItemResponse employeeItem)
        {
            var employee = new Employee(employeeItem.FirstName, employeeItem.LastName, employeeItem.Email);

            await _employeeBaseRepository.AddAsync(employee);
            return new EmployeeResponse(employee);
        }

        /// <summary>
        /// Изменение данных сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeItem"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> Update(Guid id, EmployeeItemResponse employeeItem)
        {
            var employee = await _employeeBaseRepository.GetByIdAsync(id);
            if (employee is null) return NotFound();
            
            employee.FirstName = employeeItem.FirstName;
            employee.LastName = employeeItem.LastName;
            employee.Email = employeeItem.Email;
            
            await _employeeBaseRepository.UpdateAsync(employee);
            return new EmployeeResponse(employee);
        }

        /// <summary>
        /// Удаление сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            var entity = await _employeeBaseRepository.GetByIdAsync(id);
            await _employeeBaseRepository.DeleteAsync(entity);
            
            return Ok();
        }
    }
}
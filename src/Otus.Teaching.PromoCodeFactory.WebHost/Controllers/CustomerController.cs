﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class CustomerController : ControllerBase
    {
        private readonly IEfRepository<Customer> _customerBaseRepository;
        private readonly IEfRepository<PromoCode> _promoCodeBaseRepository;

        public CustomerController(
            IEfRepository<Customer> customerBaseRepository, 
            IEfRepository<PromoCode> promoCodeBaseRepository)
        {
            _customerBaseRepository = customerBaseRepository;
            _promoCodeBaseRepository = promoCodeBaseRepository;
        }
        
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetAllCustomersAsync()
        {
            var customers = await _customerBaseRepository.GetAllAsync();
            var result = customers.Select(x => new CustomerShortResponse(x)).ToList();

            return result;
        }
        
        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomerByIdAsync(Guid id)
        {
            var customer = await _customerBaseRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            
            return new CustomerShortResponse(customer);;
        }
        
        /// <summary>
        /// Получить предпочтения клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetCustomerPreferenceById(Guid id)
        {
            var customer = await _customerBaseRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            
            return customer.CustomerPreferences.Select(s => s.Preference).ToList();;
        }

        /// <summary>
        /// Создание клиента
        /// </summary>
        /// <param name="customerItem"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerItemResponse>> Create([FromBody] CustomerItemResponse customerItem)
        {
            var customer = new Customer(customerItem.FirstName,
                customerItem.LastName,
                customerItem.Email);
            
            await _customerBaseRepository.AddAsync(customer);
            return new CustomerItemResponse(customer);
        }

        /// <summary>
        /// Изменение данных клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerItem"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<CustomerShortResponse>> Update(Guid id, CustomerItemResponse customerItem)
        {
            var customer = await _customerBaseRepository.GetByIdAsync(id);
            if (customer is null) return NotFound();

            customer.FirstName = customerItem.FirstName;
            customer.LastName = customerItem.LastName;
            customer.Email = customer.Email;
            
            await _customerBaseRepository.UpdateAsync(customer);
            return new CustomerShortResponse(customer);
        }
        
        /// <summary>
        /// Удаление клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            var customer = await _customerBaseRepository.GetByIdAsync(id);
            await _customerBaseRepository.DeleteAsync(customer);

            foreach (var promoCode in customer.PromoCodes)
            {
                await _promoCodeBaseRepository.DeleteAsync(promoCode);
            }
            
            return Ok();
        }
    }
}
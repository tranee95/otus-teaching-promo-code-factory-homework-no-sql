﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IEfRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public IEnumerable<T> Where(Func<T, bool> func);
        public T Find(Func<T, bool> func);
    }
}
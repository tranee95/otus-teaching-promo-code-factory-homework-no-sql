﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IMongoRepository<T>: IBaseRepository<T> where T : BaseEntity
    {
        Task<List<T>> Where(Expression<Func<T, bool>> func);
        Task<T> First(Expression<Func<T, bool>> func);
    }
}
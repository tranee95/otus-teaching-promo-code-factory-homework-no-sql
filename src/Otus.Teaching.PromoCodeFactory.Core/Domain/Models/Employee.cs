﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class Employee: BaseUserEntity
    {
        public Employee()
        {
        }

        public Employee(string firstName, string lastName, string email, int appliedPromocodesCount = 0) 
            : base(firstName, lastName, email)
        {
            AppliedPromocodesCount = appliedPromocodesCount;
        }
        
        public virtual Role Role { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}
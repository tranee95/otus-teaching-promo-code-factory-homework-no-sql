﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Initializers
{
    public class InitializerDb : IInitializerDb
    {
        private readonly ApplicationDbContext _context;

        public InitializerDb(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task InitializeAsync()
        {
            CreateDataBase();
            
            await AddEntitysAsync(FakeDataFactory.Employees);
            await AddEntitysAsync(FakeDataFactory.Preferences);
            await AddEntitysAsync(FakeDataFactory.Customers);
            await AddEntitysAsync(FakeDataFactory.PromoCodes);
            await AddEntitysAsync(FakeDataFactory.Partners);

            var rolesId = FakeDataFactory.Employees.Select(x => x.Role.Id).ToList();
            await AddEntitysAsync<Role>(x => !rolesId.Contains(x.Id));
        }

        private void CreateDataBase()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
        }
        
        private async Task AddEntitysAsync<T>(IEnumerable<T> entitys) where T: BaseEntity
        {
            await _context.Set<T>().AddRangeAsync(entitys);
            await _context.SaveChangesAsync();
        }

        private async Task AddEntitysAsync<T>(Func<T, bool> func) where T: BaseEntity
        {
            var entitys = _context.Set<T>().Where(func);
            await _context.AddRangeAsync(entitys);
            await _context.SaveChangesAsync();
        }
    }
}
﻿using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Initializers
{
    public class MongoInitializerDb : IInitializerDb
    {
        private readonly IMongoDatabase _database;

        public MongoInitializerDb(IMongoDatabase database)
        {
            _database = database;
        }

        public async Task InitializeAsync()
        {
            const string collectionEmployeesName = nameof(Employee) + "s";
            const string collectionRolesName = nameof(Role) + "s";
            
            await _database.DropCollectionAsync(collectionEmployeesName);
            await _database.DropCollectionAsync(collectionRolesName);
            
            var employeeCollection = _database.GetCollection<Employee>(collectionEmployeesName);
            var roleCollection = _database.GetCollection<Role>(collectionRolesName);
            await employeeCollection.InsertManyAsync(FakeDataFactory.Employees);
        }
    }
}
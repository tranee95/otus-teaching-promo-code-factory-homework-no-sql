﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Initializers
{
    public class InMemoryBaseRepository<T> : IBaseRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryBaseRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T entity)
        {
            entity.GeneratedId();

            Data = Data.Append(entity);
            return Task.FromResult(Data.First(s => s.Id == entity.Id));
        }

        public Task DeleteAsync(T entity)
        {
            var deleteEntity = Data.FirstOrDefault(s => s.Id == entity.Id);
            if (deleteEntity is null) return Task.FromResult(false);

            Data = Data.Where(s => s.Id != entity.Id);
            
            return Task.FromResult(true);
        }

        public Task UpdateAsync(T entity) => throw new NotImplementedException();
        public IEnumerable<T> Where(Func<T, bool> func) => throw new NotImplementedException();

        public T Find(Func<T, bool> func) => throw new NotImplementedException();

        public Task<T> UpdateAsync(Guid id, T entity)
        {
            if (!Data.Any(s => s.Id == entity.Id)) return null;
            Data = Data.Select(s =>  s.Id == id ? entity : s);
            
            return Task.FromResult(Data.First(s => s.Id == entity.Id));
        }
    }
}
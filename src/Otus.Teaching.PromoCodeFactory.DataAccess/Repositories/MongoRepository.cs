﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class MongoRepository<T> : IMongoRepository<T> where T: BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(IMongoCollection<T> collection)
        {
            _collection = collection;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _collection.Find(new BsonDocument()).ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _collection
                         .Find(new BsonDocument("_id", new ObjectId(id.ToString())))
                         .FirstOrDefaultAsync();

            return entity;
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _collection.DeleteOneAsync(new BsonDocument("_id", new ObjectId(entity.Id.ToString())));
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(entity.Id.ToString())), entity);
        }

        public async Task<List<T>> Where(Expression<Func<T, bool>> func)
        {
            return await _collection.Find(func).ToListAsync();
        }


        public async Task<T> First(Expression<Func<T, bool>> func)
        {
            return await _collection.Find(func).FirstOrDefaultAsync();
        }
    }
}